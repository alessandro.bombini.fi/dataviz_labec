<img src="./Assets/Pics/labec.png" alt="drawing" width="100%"/>

---

# Data Visualization 101 @ LABEC

This course is an introductory course on data visualization in Python and R held at INFN LABEC, Florence, from 24/01/2021.

The goal of the course is to furnish the basis to open, manipulate and plot data, using either Python (and its libraries) or R. 

## 1. Program of the Course 

**Note:** it has to be fully established; this is a tentative summary:

<div style='content: "";
      clear: both;
      display: table;'>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Images/ex_lec_2_1.png" alt="Snow" style="width:100%">
  </div>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Images/ex_lec_4.gif" alt="Forest" style="width:100%">
  </div>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Images/ex_lec_4_1.png" alt="Mountains" style="width:100%">
  </div>
</div>

*Small preview of the course*

------------------------------

0. **Python Basics**

    0.0. What is Python?

        0.0.1. Significant indententation

        0.0.2. Interpreted Code

        0.0.3. Very Advanced Concept: OOP

    0.1. Python 2 vs Python 3

    0.2. Basics of Python

        0.2.1. If, elif, else

        0.2.2. Iterables

        0.2.3. Navigating the File System

        0.2.4. Functions

            0.2.4.1. Typing Functions

        0.2.5. Classes

    0.3. Python vs IPython

    0.4. Python package manager - pip

        0.4.0. Importing Packages in python

    Exercises

1. **Linear Algebra in Python**

    1.0. Linear Algebra: Basics

        1.0.1. Linearity: sum of vectors, multiplication by a scalar, generic linear combination

        1.0.2. Scalar Product

        1.0.3. Linear Applications - Matrices

        1.0.4. Determinant

    1.1. Linear Algebra: solve linear systems

    1.2. Eigenvalues and Eigenvectors

2. **Dataframes in Python**

    2.1. Loading Data

        2.2.1. Creating a df from scratch and store in disk

    2.2. Basics of df

    2.3. Basis of pandas built-in statistical methods

    2.4. Sorting

    2.5. Pandas built-in plots

        2.5.1. Histogram

        2.5.2. Boxplot

        2.5.3. Other plots

    Appendix A. Scrape WikiPedia table to CSV with Pandas

3. **Basis of (1D) Data Visualization**

        3.0.1. Scatter Plot

        3.0.2. Error Plot

    3.1. Statistical Plots

        3.1.1. Violin plots

        3.1.2. Event Plots

4. **Advanced Plots - Seaborn & Plotly**

    4.0. Basics of Statistical Plots

        4.0.1. Strip Plots

        4.0.2. Swarm plots

        4.0.3. Boxplot & Boxenplot

        4.0.4. Violin Plots

        4.0.5. Wrap-up: Using statistical plots to understand the stats distribution

    4.1. 2D Plots

        4.1.2. Heatmaps

        4.1.2. Jointplots

        4.1.3. KDE plots

        4.1.4. Pairplots

        4.1.5. More on Refressions in Seaborn

    4.2. Interactive Plotting: Plotly

        4.2.1. Simple interactive scatter plot

        4.2.2. Fun with graphs I: Spider plots

        4.2.3. Fun with graphs II: Sankey (alluvial) Plots

        4.2.4. GeoPlots

5. Maniputaling Images in Python

    5.1. Manipulation

        5.1.1. Rotation

        5.1.2. Flip

        5.1.3. Resizing

        5.1.4. Exporting images

    5.2. Image Normalization

        5.2.1. Contrast stretching

        5.2.2. Histogram Equalization

    5.3. Filters
        
        5.3.1. Blurring

        5.3.2. Sharpening
        
        5.3.3. Edge detection: the Sobel algorithm

    5.4. Mathematical Morphology

        5.4.1 Erosion, Dilation

        5.4.2 Closing, Opening
        
        5.4.3. Thresholding

        5.4.4. Watershed segmentation

6. Imaging in Python

    6.0. Data Analysis of Images in python - the XRF example

        6.0.1. The Statistics of an XRF raw data

    6.1. Use Dynamical Plotting

        6.1.1. Interactive Scatter Plot

        6.1.2. Interactive Imshow

        6.1.3. Interactive 3D plot

------------------------------


## Usage - Google Colab, Drive Version

In order to properly use IPython notebooks in Google Colab (https://colab.research.google.com/?utm_source=scs-index), i.e. to access the datasets, we need to connect the Google Drive Folder; to do so, we need to mount our Drive:
(see https://stackoverflow.com/questions/48376580/google-colab-how-to-read-data-from-my-google-drive)

```python
from google.colab import drive
drive.mount('/content/drive')
```

after that we can check that we have access to the DataViz@Labec folder

```python
!ls ./drive/MyDrive/DataViz@LABEC
```
We then suggest to define a variable to be used as a base throughout the notebook:

```python
base_path = './drive/MyDrive/DataViz@LABEC/'
```

Another option (https://stackoverflow.com/questions/48905127/importing-py-files-in-google-colab) is to use ```%``` magic of jupyter to change directory:

```python
from google.colab import drive
drive.mount('/content/drive')

%cd '/content/drive/MyDrive/DataViz@LABEC/'
```

------



## Usage - Local Version

<div style='content: "";
      clear: both;
      display: table;'>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Images/gitlab_icon.png" alt="Snow" style="width:100%">
  </div>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Images/docker_logo.png" alt="Forest" style="width:100%">
  </div>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Images/docker_compose.png" alt="Mountains" style="width:100%">
  </div>
</div>

The repo can be either cloned via ```git``` as

```bash
    $ git clone https://gitlab.com/alessandro.bombini.fi/dataviz_labec.git
```

or downloaded. 

The requirements (for Python) are found in the ```requirements.txt``` file. 

### Using Virtualization with Docker & Docker Compose

The `Visualizer` can also be run inside a virtual container using `Docker` and `docker-compose`; to install Docker and docker-compose, see 

* [Install Docker engine](https://docs.docker.com/engine/install/)
* [Install Docker Compose](https://docs.docker.com/compose/install/)

On `Fedora` (32+): 

1. Install the dnf-plugins-core package (which provides the commands to manage your DNF repositories) and set up the stable repository.

```bash
$ sudo dnf -y install dnf-plugins-core
```

2. Install Docker Engine

```bash
$  sudo dnf install docker-ce docker-ce-cli containerd.io
```

3. Start docker

```bash
$  sudo systemctl start docker
```

4. Install docker-compose (see [here](https://computingforgeeks.com/install-and-use-docker-compose-on-fedora/))

```bash
$  sudo dnf -y install docker-compose
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This may not install the most current version of Docker Compose. You can check what is installed using 

```bash
$  docker-compose --version
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or the rpm command

```bash
$  rpm -qi docker-compose
```

Now it is possible to use the containerisation; 

**note**: keep an eye on the ```.env``` file where a list of environment variables is set. 

```bash
    $ docker-compose up --build
```
The jupyter notebook can be accessed via browser browsing into
```
http://127.0.0.1:8800
```
or, on Windows, 
```
http://localhost:8800
```

Users need to insert the password ```JUPYTER_TOKEN```set in the ```.env``` file.

For ```R```, instead, 
```
http://127.0.0.1:8787
```
or, on Windows, 
```
http://localhost:8787
```
Users need to insert the password ```ADMIN_PASS``` for the user ```ADMIN_USER``` set in the ```.env``` file.


## Acknowledgments 

### Datasets

- Pokemon with stats, Alberto Barradas, Kaggle, https://www.kaggle.com/abcsds/pokemon
- Il Sole 24 Ore, https://github.com/IlSole24ORE
- IntCal20, http://intcal.qub.ac.uk/intcal13/
- USA Elections, Ethan Schacht, Kaggle, https://www.kaggle.com/etsc9287/2020-general-election-polls
- Animal Crossing, Jessica Li, Kaggle, https://www.kaggle.com/jessicali9530/animal-crossing-new-horizons-nookplaza-dataset
- Natural Images, Prasun Roy, Kaggle, https://www.kaggle.com/prasunroy/natural-images


### Partners
<div style='content: "";
      clear: both;
      display: table;'>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Pics/EOSc-Pillar_logo_final.png" alt="Snow" style="width:100%">
  </div>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Pics/4CH_LOGO_wTEXT.png" alt="Forest" style="width:80%;">
  </div>
  <div style="float: left;
      width: 31%;
      padding: 5px;">
    <img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Pics/Ariadne_plus_logo.jpg" alt="Mountains" style="width:100%">
  </div>
</div>

----
<img src="https://gitlab.com/alessandro.bombini.fi/dataviz_labec/-/raw/master/Assets/Pics/CHNet_Banner_4Ch.png" alt="drawing" width="100%"/>