# NVIDIA CUDA image as a base
# We also mark this image as "jupyter-base" so we could use it by name
#FROM nvidia/cuda:10.2-runtime AS jupyter-base
#FROM nvidia/cuda:10.1-runtime AS jupyter-base
FROM nvidia/cuda:10.1-cudnn7-runtime AS jupyter-base


WORKDIR /
# Install Python and its tools
RUN apt update && apt install -y --no-install-recommends \
    git \
    build-essential \
    python3-dev \
    python3-pip \
    python3-setuptools

RUN pip3 -q install pip --upgrade

# install graphviz
#RUN sudo apt-get install graphviz

#Copy the requirements.txt file to the container 
COPY requirements.txt /

#Installing python packages 
RUN pip3 install -r requirements.txt

## Additional requiorements
#Copy the requirements.txt file to the container 
COPY additional_requirements.txt /

#Installing python packages 
RUN pip3 install -r additional_requirements.txt